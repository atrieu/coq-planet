open Pl_anet

let itemize items = Lwt_main.run (Common.itemize items)

let coq () =
  let html = itemize
    [ Discourse.generate_html ~url:"https://coq.discourse.group"
    ; Coqclub.generate_html
    ; Stackoverflow.SO.generate_html ~tag:"coq"
    ; Stackoverflow.TCS.generate_html ~tag:"coq"
    ; Reddit.generate_html ~subreddit:"Coq"
    ; Zulip.generate_html
    ] in
  let body = Html.to_string html in
  Template.run ~topic:"coq" ~body

let haskell () =
  let url l = "https://mail.haskell.org/pipermail/" ^ l in
  let haskell_cafe = Mailman.generate_html
        ~name:"The Haskell Café mailing list"
        ~url:(url "haskell-cafe")
        ~unique_id:"haskell-cafe" in
  let libraries = Mailman.generate_html
        ~name:"The Libraries mailing list"
        ~url:(url "libraries")
        ~unique_id:"haskell-libraries" in
  let html = itemize
    [ Reddit.generate_html ~subreddit:"haskell"
    ; haskell_cafe
    ; libraries
    ; Stackoverflow.SO.generate_html ~tag:"haskell"
    ; Discourse.generate_html ~url:"https://discourse.haskell.org"
    ] in
  let body = Html.to_string html in
  Template.run ~topic:"haskell" ~body

let main () =
  Opt.parse_args ();
  coq ();
  haskell ();
  ()

let () =
  match main () with
  | () -> Debug.clean_up ()
  | exception e ->
    Debug.dump ();
    raise e
