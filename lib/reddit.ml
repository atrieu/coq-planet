open Lwt
open Common

(* Reddit API doc: https://www.reddit.com/dev/api *)
let sub_url ~subreddit = "https://www.reddit.com/r/" ^ subreddit
let api_url ~subreddit = sub_url ~subreddit ^ "/new.json?limit=10"

type meta =
  { comments : int
  }

let render_meta m =
  m.comments
    |> show_count "comment"
    |> Option.map Html.escape

let fetch ~url () =
  Common.get_gzip url >|= fun body ->
  let json = Yojson.Basic.from_string body in
  let open Yojson.Basic.Util in
  let to_post j =
    let j = j |> member "data" in
    { title = j |> member "title" |> to_string
    ; url = j |> member "permalink" |> to_string |> ((^) "https://www.reddit.com")
    ; time = j |> member "created" |> to_float |> Time.time_of_unix_float
    ; meta =
        { comments = j |> member "num_comments" |> to_int }
    }
  in
  json
    |> member "data"
    |> member "children"
    |> to_list
    |> take 10
    |> List.map to_post

let source ~subreddit =
  { name = "Reddit"
  ; url = sub_url ~subreddit
  ; unique_id = "reddit-" ^ subreddit
  ; fetch = fetch ~url:(api_url ~subreddit)
  }

let generate_html ~subreddit () : Html.t Lwt.t =
  Common.generate_html (source ~subreddit) ~escape_title:false render_meta
