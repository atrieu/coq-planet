open Lwt
open Common

(* Common API all of Stack Exchange. We fetch both Stack Overflow and CS Theory Stack Exchange. *)

(* StackExchange API doc: https://api.stackexchange.com/docs/questions *)
let mk_url ~tag ~site =
  "https://api.stackexchange.com/2.2/questions?pagesize=10&order=desc&sort=activity&tagged=" ^ tag ^ "&site=" ^ site

(* +dependent-type+proof-assistants *)

type meta =
  { answers : int
  ; creation : Time.time
  }

type question = meta Common.entry

let print_question q =
  let print_meta meta =
    let s = string_of_int meta.answers in
    s ^ " answers" in
  print_entry print_meta q

let render_meta m : Html.t option =
  m.answers
    |> show_count "answer"
    |> Option.map Html.escape

(**)

let fetch ~url () =
  Common.get_gzip url >|= fun body ->
  let json = Yojson.Basic.from_string body in
  let open Yojson.Basic.Util in
  let to_question j =
    { title = j |> member "title" |> to_string
    ; url = j |> member "link" |> to_string
    ; time = j |> member "last_activity_date" |> to_int |> Time.time_of_unix
    ; meta =
        { answers = j |> member "answer_count" |> to_int
        ; creation = j |> member "creation_date" |> to_int |> Time.time_of_unix
        }
    } in
  json
    |> member "items"
    |> to_list
    |> List.map to_question

(**)

module SO = struct

let source ~tag =
  { name = "Stack Overflow"
  ; url = "https://stackoverflow.com/questions/tagged/" ^ tag
  ; unique_id = "stackoverflow-" ^ tag
  ; fetch = fetch ~url:(mk_url ~tag ~site:"stackoverflow")
  }

let generate_html ~tag () : Html.t Lwt.t =
  (* StackOverflow's API already gives escaped titles *)
  Common.generate_html (source ~tag) ~escape_title:false render_meta

end

module TCS = struct

let source ~tag =
  { name = "CS Theory Stack Exchange"
  ; url = "https://cstheory.stackexchange.com/questions/tagged/" ^ tag
  ; unique_id = "cstheory-" ^ tag
  ; fetch = fetch ~url:(mk_url ~tag ~site:"cstheory")
  }

let generate_html ~tag () : Html.t Lwt.t =
  (* StackOverflow's API already gives escaped titles *)
  Common.generate_html (source ~tag) ~escape_title:false render_meta

end
