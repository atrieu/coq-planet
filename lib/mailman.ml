open Lwt
open Common
open Time

let mk_url ~url (Year year) month ext =
  url ^ "/" ^ string_of_int year ^ "-" ^ name_of_month month ^ ext

type entry = agg_meta Common.entry

let print_entry e =
  Common.print_entry (fun meta -> string_of_int meta.response_count) e

let render_meta meta : Html.t option =
  meta.response_count
    |> show_count "reply" ~plural:"replies"
    |> Option.map Html.escape

let read_time (d : string) : time =
  Scanf.sscanf d "%_4s %2d %3s %4d %2d:%2d:%_2d"
    @@ fun cal_day mm cal_year tod_hour tod_minute ->
      { time_day =
          { cal_day
          ; cal_month = month_of_name_ mm
          ; cal_year
          }
      ; time_clock = Some { tod_hour ; tod_minute }
      }

(* There are newlines and tabs *)
let rex_spaces = Re.Pcre.regexp "\\s+"
let clean_whitespace s =
  s |> String.trim
    |> Re.Pcre.substitute ~rex:rex_spaces ~subst:(fun _ -> " ")

let subject_rex_tag = Re.Pcre.regexp "\\[Haskell-cafe\\] "
let clean_subject_tag s =
  Re.Pcre.substitute ~rex:subject_rex_tag ~subst:(fun _ -> "") s

let clean_subject s = s |> clean_subject_tag |> clean_whitespace

let rex_headers = Re.Pcre.regexp ~flags:[`MULTILINE]
  "^Date:[ \t]*([^\n]*)\nSubject:[ \t]*(?:[^\n]*(?:\n[ \t]+[^\n]*)*)"

let process_txt body : time list =
  let open Re.Pcre in
  let rec matches ~pos acc =
    match exec ~pos ~rex:rex_headers body with
    | g ->
      let open Re.Group in
      let e = read_time (get g 1) in
      matches ~pos:(stop g 0) (e :: acc)
    | exception Not_found -> acc
  in matches ~pos:0 []
;;

type light_entry = { title : string ; url : string }

let from_anchor ~url year month t =
  let open Soup in
  let t = t |> R.tag "a" in
  let title = t |> R.leaf_text |> clean_subject in
  let url = mk_url ~url year month ("/" ^ R.attribute "href" t) in
  { title ; url }

let scrape_html ~url year month body : light_entry list =
  let open Soup in
  body |> parse $$ "ul"
    |> R.nth 2
    |> children
    |> elements
    |> to_list
    |> List.rev
    |> List.map (from_anchor ~url year month)

let rec zip_with f xs ys =
  match xs, ys with
  | x :: xs, y :: ys ->
    let z = f x y in z :: zip_with f xs ys
  | _, _ -> []
;;

let fetch_ ~url year month =
  let get ext = get_gzip (mk_url ~url year month ext) in
  let get_dates = get ".txt" >|= process_txt in
  let get_urls = get "/date.html" >|= scrape_html ~url year month in
  get_dates >>= fun dates ->
  get_urls >>= fun urls ->
  let combine time { title ; url } =
    { title ; url ; time ; meta = () } in
  flush stdout;
  Lwt.return (zip_with combine dates urls)

let fetch ~url () =
  let (year, month) = current_ym () in
  let get_msg_curr_month = fetch_ ~url year month in
  let get_msg_prev_month =
    let (year, month) = previous_ym year month in
    fetch_ ~url year month in
  get_msg_curr_month >>= fun msg_curr_month ->
  get_msg_prev_month >|= fun msg_prev_month ->
  let msgs = msg_curr_month @ msg_prev_month in
  msgs
    |> Common.aggregate_entries

let generate_html ~name ~url ~unique_id () =
  let source = { name ; url ; unique_id ; fetch = fetch ~url } in
  Common.generate_html source render_meta
