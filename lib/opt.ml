let _skip_coqclub = ref false
let skip_coqclub () = !_skip_coqclub

let parse_arg arg =
  match arg with
  | "--skip-coqclub" -> _skip_coqclub := true
  | _ -> failwith ("Unknown argument: " ^ arg)
;;

let parse_args () =
  for i = 1 to Array.length Sys.argv - 1 do
    parse_arg Sys.argv.(i)
  done
