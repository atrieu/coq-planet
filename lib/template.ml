type path = string

type locations =
  { index_from : path
  ; style_from : path
  ; config_dir : path
  ; config_file : path
  ; assets_from : path
  ; output_dir : path
  ; index_to : path
  ; assets_to : path
  ; style_to : path
  }

(* All file locations are defined upfront here. *)
let locations ~topic =
  let config_dir = "config/" ^ topic in
  let output_dir = "output/" ^ topic in
  { index_from = "template/index.html"
  ; style_from = "template/style.css"
  ; config_dir
  ; config_file = config_dir ^ "/config.json"
  ; assets_from = config_dir ^ "/assets"
  ; output_dir
  ; index_to = output_dir ^ "/index.html"
  ; assets_to = output_dir ^ "/assets"
  ; style_to = output_dir ^ "/style.css"
  }

let with_output file print_to =
  let out = open_out file in
  let fmt = Format.formatter_of_out_channel out in
  print_to fmt;
  Format.pp_print_flush fmt ();
  close_out out

let read_template file =
  let inchan = open_in file in
  let lexbuf = Lexing.from_channel inchan in
  let t = Mustache.parse_lx lexbuf in
  close_in inchan;
  t

let template ~src ~dst env =
  let t = read_template src in
  with_output dst (fun out ->
    Mustache.render_fmt ~strict:true out t env)

let read_json_ file =
  let inchan = open_in file in
  let json = Ezjsonm.from_channel inchan in
  close_in inchan;
  json

let read_json file =
  try read_json_ file with
  | e -> prerr_endline ("Error while reading " ^ file); raise e

let as_object v =
  match v with
  | `O obj -> obj
  | _ -> invalid_arg "This is not a JSON object"

let member : string -> (string * Ezjsonm.value) list -> Ezjsonm.value = List.assoc
let member_opt : string -> (string * Ezjsonm.value) list -> Ezjsonm.value option = List.assoc_opt

let fmt_more_links j =
  let open Ezjsonm in
  let open Html in
  let list_items = Ezjsonm.get_list @@ fun j ->
    let obj = as_object j in
    a ~attrs:[href_ (obj |> member "url" |> get_string)]
      (obj |> member "name" |> get_string |> escape) in
  match j |> list_items with
  | [] -> Html.empty
  | xs -> concat [p (escape "More links:"); ul (List.map li xs)]
;;

let clean_config config =
  let config = as_object config in
  let open Ezjsonm in
  let topic = config |> member "topic" |> get_string in
  let raw_topic = topic |> Soup.parse |> Soup.texts |> String.concat "" in
  let more_links =
    match config |> member_opt "more-links" with
    | None -> string ""
    | Some more -> more |> fmt_more_links |> Html.to_string |> string in
  let config = List.remove_assoc "more-links" config in
  (  ("raw-topic", string raw_topic)
  :: ("more-links", more_links)
  :: config
  )

let current_time () =
  let now = Ptime_clock.now () in
  let b = Buffer.create 30 in
  let fmt = Format.formatter_of_buffer b in
  Ptime.pp fmt now;
  Format.pp_print_flush fmt ();
  Buffer.contents b

let mk_env ~body =
  [ ("body", `String body)
  ; ("update", `String (current_time ()))
  ]

let cpdir ?(ignore_missing_dir=false) src dst =
  let open FileUtil in
  if ignore_missing_dir && not (test Is_dir src) then ()
  else (
    mkdir ~parent:true dst;
    cp (ls src) dst
  )

let skip_if_doesnotexist msg err =
  match err with
  | `NoSourceFile _ -> ()
  | _ -> raise (FileUtil.CpError msg)
;;

let run_ locs env =
  let config = read_json locs.config_file |> clean_config in
  let env = `O (env @ config) in
  FileUtil.mkdir ~parent:true locs.output_dir;
  template ~src:locs.index_from ~dst:locs.index_to env;
  template ~src:locs.style_from ~dst:locs.style_to env;
  cpdir ~ignore_missing_dir:true locs.assets_from locs.assets_to

let run ~topic ~body =
  run_ (locations ~topic) (mk_env ~body)
