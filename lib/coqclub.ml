open Lwt
open Cohttp
open Common
open Time

let pp node =
  print_endline (Soup.pretty_print node)

(** Output type *)

type entry = agg_meta Common.entry

let print_entry e =
  Common.print_entry (fun meta -> string_of_int meta.response_count) e

let render_meta meta : Html.t option =
  meta.response_count
    |> show_count "reply" ~plural:"replies"
    |> Option.map Html.escape

(** Current date *)

(** HTTP Coq Club *)

(* Scrape this *)
let mk_url ~year ~month ~basename =
  "https://sympa.inria.fr/sympa/arc/coq-club/"
    ^ string_of_year year ^ "-" ^ string_of_month month ^ "/" ^ basename

let initial_basename = "mail1.html"

type cookie_state =
  | NoCookie
  | WaitCookie of string Lwt_condition.t
  | Cookie of string

let cookie = ref NoCookie

(* Only one thread initially works to get a cookie *)
let get_cookie () =
  match !cookie with
  | NoCookie ->
    cookie := WaitCookie (Lwt_condition.create ());
    Lwt.return None
  | WaitCookie k -> Lwt_condition.wait k >|= fun c -> Some c
  | Cookie c -> Lwt.return (Some c)

let set_cookie h =
  let c = Header.get h "set-cookie" |> Option.get in
  (match !cookie with
  | WaitCookie k -> Lwt_condition.broadcast k c
  | _ -> ());
  cookie := Cookie c

(* HTTP GET request a Coq Club archive URL. *)
(* Coq Club requires a cookie for access so we set it and retry if we get a 302. *)
let rec get_coqclub ?(retries=2) url =
  get_cookie () >>= fun c ->
  let headers = Option.map (Header.init_with "cookie") c in
  Common.get ?headers url >>= fun (res, body) ->
  match Response.status res with
  | `OK -> Lwt.return body
  | `Found
  | `Temporary_redirect ->
    if retries = 0 then
      failwith "GET FAILED"
    else (
      res |> Response.headers |> set_cookie;
      get_coqclub ~retries:(retries-1) url
    )
  | _ -> failwith "BAD URL"
;;

(** Scraping *)

(* Unpack a 1-element list *)
let expect_single xs =
  match xs with
  | x :: [] -> x
  | x :: _ :: _ ->
    print_endline "Warning: Expected one element";
    x
  | [] -> failwith "no element found"
;;

(* Grab the children of the [<ul>] element containing the list of emails *)
let _find_mail_list body =
  let open Soup in
  body $ ".block"
    |> children |> elements
    |> to_list |> List.filter (fun n -> name n = "ul") |> expect_single
    |> children |> elements
    |> to_list

let find_mail_list body =
  match Soup.(body $? "#ErrorMsg") with
  | None -> Some (_find_mail_list body)
  | Some _ -> None

(* Process one individual email *)
let mail_node ~year ~month time html =
  let mk_url basename = mk_url ~year ~month ~basename in
  let open Soup in
  let title = html |> children |> elements |> R.first in
  { time
  ; title = title |> R.leaf_text
  ; url = title |> R.tag "a" |> R.attribute "href" |> mk_url
  ; meta = ()
  }
;;

(* Process one day *)
let day_node ~year ~month html =
  let open Soup in
  match html
    |> children |> elements
    |> to_list
  with
  | [date; mails] ->
    let date = date |> R.leaf_text |> parse_yymmdd |> day_time in
    mails
      |> children |> elements
      |> to_list
      |> List.map (mail_node ~year ~month date)
  | bad_nodes ->
    print_endline "This is wrong";
    Printf.printf "%d elements (expected 2)\n" (List.length bad_nodes);
    bad_nodes |> List.iter (fun node ->
      print_endline (pretty_print node));
    failwith "unexpected node"

(* Find the link to the next page *)
let next_mail body =
  let open Soup in
  body $$ ".ArcMenuLinks"
    |> filter (fun x -> R.leaf_text x = ">")
    |> first
    |> Option.map (attribute "href") |> Option.join

(* year: 4-digit string, e.g., "2020"
   month: 2-digit string, e.g., "07" *)
let rec _get_coqclub_list ?(all_mails=[]) ~year ~month ~basename () =
  let url = mk_url ~year ~month ~basename in
  get_coqclub url >>= fun body ->
  let body = Soup.parse body in
  let next = next_mail body in
  match find_mail_list body with
  | None -> Lwt.return all_mails
  | Some mails ->
    let mails = mails |> List.concat_map (day_node ~year ~month) in
    let all_mails = List.rev mails @ all_mails in
    match next with
    | None -> Lwt.return all_mails
    | Some basename' when basename' = basename -> Lwt.return all_mails (* Just in case *)
    | Some basename ->
      _get_coqclub_list ~all_mails ~year ~month ~basename ()
;;

let get_coqclub_list ~year ~month =
  _get_coqclub_list ~year ~month ~basename:initial_basename ()

(**)

(* Take [n] elements from [xs] and, if there aren't enough, the lazy list [more]. *)
let rec take n xs more =
  match xs with
  | _ when n = 0 -> []
  | x :: xs ->
    x :: take (n-1) xs more
  | [] ->
    match more with
    | None -> []
    | Some f -> take n (f ()) None
;;

let rex_noise = Re.Pcre.regexp "Re: |\\[Coq-Club\\] "
let canonize = Re.Pcre.substitute ~rex:rex_noise ~subst:(fun _ -> "")

(* Scraping CoqClub is slow so we have an option --skip-coqclub to skip it. *)
exception SkipCoqClub

(* Get the last 10 emails *)
let fetch () =
  if Opt.skip_coqclub () then raise SkipCoqClub;
  let (year, month) = current_ym () in
  let get_msg_curr_month = get_coqclub_list ~year ~month in
  let get_msg_prev_month =
    let (year, month) = previous_ym year month in
    get_coqclub_list ~year ~month in
  get_msg_curr_month >>= fun msg_curr_month ->
  get_msg_prev_month >|= fun msg_prev_month ->
  let msgs = msg_curr_month @ msg_prev_month in
  msgs
    |> Common.aggregate_entries
    |> List.map (fun e -> { e with title = canonize e.title })

let source =
  { name = "The Coq Club mailing list"
  ; url = "https://sympa.inria.fr/sympa/arc/coq-club/"
  ; unique_id = "coqclub"
  ; fetch
  }

let generate_html () = Common.generate_html source render_meta
