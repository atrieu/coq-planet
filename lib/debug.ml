type logger = out_channel -> unit

let debug_actions : logger Queue.t = Queue.create ()

let debug (f : logger) : unit =
  Queue.push f debug_actions

(* Call signal_problem to remember an error that we recovered from,
   so we can report it at the end of the program. *)
let problems = ref ([] : string list)

let signal_problem info = problems := (info :: !problems)

let dump () =
  let logfile = "error.log" in
  prerr_endline ("Dumping errors to " ^ logfile);
  let errchan = open_out logfile in
  let rec _dump () =
    match Queue.take_opt debug_actions with
    | None -> ()
    | Some log -> log errchan; _dump ()
  in
  _dump ();
  close_out errchan

let clean_up () =
  match !problems with
  | [] -> ()
  | _ :: _ ->
    dump ();
    List.iter (fun x -> prerr_endline ("ERROR: " ^ x)) !problems;
    problems := []
;;
