(**)

type calendar_day =
  { cal_year : int
  ; cal_month : int
  ; cal_day : int
  }

type time_of_day =
  { tod_hour : int
  ; tod_minute : int
  }

type time =
  { time_day : calendar_day
  ; time_clock : time_of_day option
  }

let day_time time_day = { time_day ; time_clock = None }

(**)

type year = Year of int
type month = Month of int (* 1-indexed *)
type day = Day of int     (* 1-indexed *)

let previous_ym (Year y) (Month m) =
  if m == 1 then
    (Year (y-1), Month 12)
  else
    (Year y, Month (m-1))

let current_time () =
  let t = Unix.gettimeofday () |> Unix.gmtime in
  { time_day =
      { cal_year = 1900 + t.tm_year
      ; cal_month = 1 + t.tm_mon
      ; cal_day = t.tm_mday
      }
  ; time_clock = Some
      { tod_hour = t.tm_hour
      ; tod_minute = t.tm_min
      }
  }

let current_ym () =
  let t = current_time () in
  (Year t.time_day.cal_year, Month t.time_day.cal_month)

let string_of_year (Year n) = Printf.sprintf "%04d" n
let string_of_month (Month n) = Printf.sprintf "%02d" n

let mk_calendar_day (Year cal_year) (Month cal_month) (Day cal_day) =
  { cal_year ; cal_month ; cal_day }

(**)

let day_of_the_week d =
  match Ptime.of_date (d.cal_year, d.cal_month, d.cal_day) with
  | None -> ""
  | Some d ->
    match Ptime.weekday d with
    | `Mon -> "Mon"
    | `Tue -> "Tue"
    | `Wed -> "Wed"
    | `Thu -> "Thu"
    | `Fri -> "Fri"
    | `Sat -> "Sat"
    | `Sun -> "Sun"
;;

let name_of_month (Month m) =
  match m with
  | 1 -> "January"
  | 2 -> "February"
  | 3 -> "March"
  | 4 -> "April"
  | 5 -> "May"
  | 6 -> "June"
  | 7 -> "July"
  | 8 -> "August"
  | 9 -> "September"
  | 10 -> "October"
  | 11 -> "November"
  | 12 -> "December"
  | _ -> assert false
;;

let month_of_name_ name =
  match name with
  | "Jan" -> 1
  | "Feb" -> 2
  | "Mar" -> 3
  | "Apr" -> 4
  | "May" -> 5
  | "Jun" -> 6
  | "Jul" -> 7
  | "Aug" -> 8
  | "Sep" -> 9
  | "Oct" -> 10
  | "Nov" -> 11
  | "Dec" -> 12
  | _ -> assert false

let month_of_name name = Month (month_of_name_ name)

(**)

(* "YYYY-MM-DD[T]HH-MM" *)
let parse_iso (s : string) = Scanf.sscanf s "%4d-%2d-%2dT%2d:%2d"
  (fun cal_year cal_month cal_day tod_hour tod_minute ->
    { time_day = { cal_year ; cal_month ; cal_day }
    ; time_clock = Some { tod_hour ; tod_minute }
    })

(* From 2-digit to 4-digit year *)
let full_year y =
  if y >= 100 then failwith "unexpected year, some format probably changed"
  else if y >= 93 then 1900 + y
  else 2000 + y

(* From 4-digit to 2-digit year *)
let truncate_year y =
  if y >= 2000 then y - 2000 else y - 1900

(* "YY/MM/DD" *)
let parse_yymmdd (s : string) : calendar_day =
  Scanf.sscanf s "%2d/%2d/%2d" (fun y cal_month cal_day ->
    { cal_year = full_year y ; cal_month ; cal_day })

let string_of_yymmdd d =
  Printf.sprintf "%02d/%02d/%02d" (truncate_year d.cal_year) d.cal_month d.cal_day

let string_of_yyyymmdd d =
  Printf.sprintf "%04d/%02d/%02d" d.cal_year d.cal_month d.cal_day

let time_of_unix_ (mk_span : 'a -> Ptime.span) (t : 'a) =
  match t |> mk_span |> Ptime.of_span with
  | None -> assert false
  | Some p ->
    let ((cal_year, cal_month, cal_day), ((tod_hour, tod_minute, _), _)) =
      Ptime.to_date_time p in
    { time_day = { cal_year ; cal_month ; cal_day }
    ; time_clock = Some { tod_hour ; tod_minute }
    }
;;

let time_of_unix = time_of_unix_ Ptime.Span.of_int_s
let time_of_unix_float = time_of_unix_ (fun t -> t |> Ptime.Span.of_float_s |> Option.get)

(**)

let compare = (compare : time -> time -> int)

module Infix = struct
  let (<=) = ((<=) : time -> time -> bool)
end
