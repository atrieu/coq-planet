open Lwt
open Common

(* Discourse API doc: https://docs.discourse.org/ *)
let topics_url ~url = url ^ "/latest.json?order=created"

(**)

type meta =
  { replies : int
  }

type entry = meta Common.entry

let print_entry e =
  let print_meta meta =
    string_of_int meta.replies ^ " replies" in
  Common.print_entry print_meta e

let render_meta (m : meta) : Html.t option =
  m.replies
    |> show_count "answer"
    |> Option.map Html.escape

(**)

let link_from_id i =
  "https://coq.discourse.group/t/" ^ string_of_int i

let fetch ~topics_url () =
  Common.get_gzip topics_url >|= fun body ->
  let json = Yojson.Basic.from_string body in
  let open Yojson.Basic.Util in
  let to_topic j =
    { title = j |> member "title" |> to_string
    ; url = j |> member "id" |> to_int |> link_from_id
    ; time = j |> member "last_posted_at" |> to_string |> Time.parse_iso
    ; meta =
        { replies = j |> member "posts_count" |> to_int |> (fun x -> x - 1)
        }
    } in
  let topics = json
    |> member "topic_list" |> member "topics" |> to_list
    |> take 10
    |> List.map to_topic
    |> List.sort desc_time in
  topics

let source ~url =
  { name = "Discourse"
  ; url
  ; unique_id = "discourse-" ^ string_of_int (Hashtbl.hash url)
  ; fetch = fetch ~topics_url:(topics_url ~url)
  }

let generate_html ~url () : Html.t Lwt.t = Common.generate_html (source ~url) render_meta
