let secrets_file = "config/secrets.json"

let secrets = lazy (
  let f = open_in secrets_file in
  let s = really_input_string f (in_channel_length f) in
  close_in f;
  s |> Yojson.Basic.from_string)

let get_auth_ service =
  let open Yojson.Basic.Util in
  let j = secrets |> Lazy.force |> member service in
  let user = j |> member "user" |> to_string in
  let key = j |> member "key" |> to_string in
  if key = "" then failwith "Empty key: remember to write your credentials to secrets.json";
  `Basic (user, key)

let get_auth service =
  try get_auth_ service with
  | e ->
    Printf.fprintf stderr
      "Fetching secrets failed: expected file 'secrets.json' with key '%s' containing object with keys 'user' and 'key'"
      service;
    raise e
