open Lwt
open Cohttp
open Common

let url_get_stream_id = "https://coq.zulipchat.com/api/v1/get_stream_id?stream=Coq+users"
let coq_users_id = "237977"

let url_topics i = "https://coq.zulipchat.com/api/v1/users/me/" ^ i ^ "/topics"
let url = url_topics coq_users_id

type entry = { title : string }

let is_basicchar c =
     c = '-'
  || ('a' <= c && c <= 'z')
  || ('A' <= c && c <= 'Z')
  || ('0' <= c && c <= '9')

let hex_char i =
  if i < 0 || 16 <= i then invalid_arg "Expected hex digit"
  else if i <= 9 then Char.chr (i + Char.code '0')
  else Char.chr (i - 10 + Char.code 'A')

let discourse_slug title =
  let b = Buffer.create 100 in
  let escape_char c =
    if is_basicchar c then Buffer.add_char b c
    else
      let (x, y) = (Char.code c / 16, Char.code c mod 16) in
      Buffer.add_char b '.';
      Buffer.add_char b (hex_char x);
      Buffer.add_char b (hex_char y)
  in
  String.iter escape_char title;
  Buffer.contents b

let topic_url title = "https://coq.zulipchat.com/#narrow/stream/237977/topic/" ^ discourse_slug title

let render_entry { title } : Html.t =
  let open Html in
  Common.title_div ~title
    (a ~attrs:[href_ (topic_url title)]
      (escape title))

let fetch ~auth () =
  let headers = Header.add_authorization (Header.init ()) auth in
  Common.get_gzip ~headers url >|= fun body ->
  let json = Yojson.Basic.from_string body in
  let open Yojson.Basic.Util in
  json
    |> member "topics"
    |> to_list
    |> List.map (fun j -> j |> member "name" |> to_string)
    |> List.filter (fun title ->
         title <> "New Stack Exchange question" &&
         title <> "New Discourse topics")
    |> Common.take 10
    |> List.map (fun title -> { title })

let fetch () =
  let auth = Secrets.get_auth "zulip" in
  fetch ~auth ()

let src =
  { name = "Zulip"
  ; url = "https://coq.zulipchat.com"
  ; unique_id = "zulip-coq"
  ; fetch
  }

let generate_html () = Common.core_generate_html ~info:"requires login" src render_entry
