open Lwt
open Cohttp
module Client = Cohttp_lwt_unix.Client

let debug = Debug.debug

(* HTTP Requests *)

let get ?headers url =
  print_endline url;
  Client.get ?headers (Uri.of_string url) >>= fun (resp, body) ->
  let code = resp |> Response.status |> Code.code_of_status in
  body |> Cohttp_lwt.Body.to_string >|= fun body ->
    debug Printf.(fun h ->
      fprintf h "%s\n" url;
      fprintf h "Response code: %d\n" code;
      fprintf h "Headers: %s\n" (resp |> Response.headers |> Header.to_string);
      fprintf h "Body of length: %d\n" (String.length body));
    (resp, body)

let get_body ?headers url =
  get ?headers url >>= fun (resp, body) ->
  match Response.status resp with
  | `OK -> Lwt.return body
  | _ -> failwith "Error: GET failed"

let get_gzip ?headers url =
  let headers = Header.add_opt headers "accept-encoding" "gzip" in
  get_body ~headers url >>= fun body ->
  match Ezgzip.decompress body with
  | Ok body -> Lwt.return body
  | Error _ -> failwith "Error: decompress failed"

(**)

type 'meta entry =
  { title : string
  ; url : string
  ; time : Time.time
  ; meta : 'meta
  }

let desc_time e1 e2 = Time.compare e2.time e1.time

let print_entry (print_meta : 'meta -> string) (e : 'meta entry) =
  Printf.printf "%s (%s; %s) | %s\n"
    e.title
    (Time.string_of_yymmdd e.time.time_day)
    (print_meta e.meta)
    e.url

let render_time (t : Time.time) =
  let open Html in
  let dow = Time.day_of_the_week t.time_day in
  let ymd = Time.string_of_yyyymmdd t.time_day in
  let timestring = dow ^ " " ^ ymd in
  span ~attrs:[class_ "item-time"; title_ timestring] (escape timestring)

let title_div ~title = Html.div ~attrs:[Html.class_ "item-title item-part"; Html.title_ title]
let meta_div = Html.div ~attrs:[Html.class_ "item-part item-part-meta item-before-sep"]

let render_entry
      (render_meta : 'meta -> Html.t option)
      ?(render_time : Time.time -> Html.t = render_time)
      ?(escape_title = true)
      (e : 'meta entry)
  : Html.t =
  let open Html in
  let title = if escape_title then escape e.title else unsafe_raw e.title in
  let title = title_div ~title:e.title (a ~attrs:[("href", `Str e.url)] title) in
  let meta =
        match render_meta e.meta with
        | None -> empty
        | Some meta -> meta_div meta in
  let time = meta_div (render_time e.time) in
  concat [title; meta; time]

(**)

type 'entry source =
  { name : string
  ; url : string
  ; unique_id : string
    (* Uniquely identify both the source and the topic,
       to serve as the name of the cache. *)
  ; fetch : unit -> 'entry list Lwt.t
  }

(**)

type agg_meta = { mutable response_count : int }

let aggregate_entries ?(max_entries=10) (es : 'a entry list) : agg_meta entry list =
  let seen : (string, agg_meta entry) Hashtbl.t = Hashtbl.create max_entries in
  let eat e =
    match Hashtbl.find_opt seen e.title with
    | None when Hashtbl.length seen < max_entries ->
      Hashtbl.add seen e.title {e with meta = { response_count = 0 } }
    | None -> ()
    | Some e' -> e'.meta.response_count <- 1 + e'.meta.response_count
  in
  List.iter eat es;
  seen |> Hashtbl.to_seq_values |> List.of_seq |> List.sort desc_time

(**)

let slug = String.map (fun c -> if c = ' ' then '-' else c)

let core_generate_html
      ?info
      (src : 'entry source)
      (render_entry : 'entry -> Html.t)
  : Html.t Lwt.t =
  src.fetch () >|= fun es ->
  let open Html in
  let h2_attrs = [id_ ("source-" ^ slug src.name)] in
  let h2_attrs =
        match info with
        | None -> h2_attrs
        | Some info -> title_ ("(" ^ info ^ ")") :: class_ "source-info" :: h2_attrs in
  concat
    [ h2 ~attrs:h2_attrs
        (a ~attrs:[("href", `Str src.url)] (escape src.name))
    ; ul ~attrs:[class_ "link-table"]
        (es |> List.map
          (fun e -> li ~attrs:[class_ "item-line"] (render_entry e)))
    ]

let generate_html_
      (src : 'a entry source)
      ?(render_time : (Time.time -> Html.t) option)
      ?(escape_title = true)
      (render_meta : 'a -> Html.t option)
  : Html.t Lwt.t =
  core_generate_html src (render_entry ?render_time ~escape_title render_meta)

(* If a source goes down or causes some problem, we don't want to kill the
   whole program over it. Whenever we succeed we record the generated HTML in a cache,
   so in case of failure we just reuse the last result. *)
let cached name (get_t : unit -> Html.t Lwt.t) : Html.t Lwt.t =
  Lwt.try_bind get_t
    (fun t ->
      FileUtil.mkdir ~parent:true "cache";
      let o = open_out name in
      output_string o (Html.to_string t);
      close_out o;
      Lwt.return t)
    (fun e ->
      let e = Printexc.to_string e in
      Debug.signal_problem ("while processing source " ^ name);
      Debug.debug (fun h ->
        Printf.fprintf h "Could not process source: %s\n" e;
        Printf.fprintf h "Using cache %s\n" name;
        output_char h '\n');
      let h = open_in name in
      let s = really_input_string h (in_channel_length h) in
      close_in h;
      Lwt.return (Html.unsafe_raw s))

let generate_html src ?render_time ?escape_title render_meta : Html.t Lwt.t =
  let name = "cache/" ^ src.unique_id ^ ".html" in
  cached name (fun () ->
    generate_html_ src ?render_time ?escape_title render_meta)

(**)

let itemize (items : (unit -> Html.t Lwt.t) list) : Html.t Lwt.t =
  let open Html in
  let item = div ~attrs:[class_ "grid-item"] in
  items |> Lwt_list.map_p (fun run -> run () >|= item) >|= fun items ->
  div ~attrs:[class_ "grid-box"] (Html.concat items)

(**)

let run f x = f x; x

let show_count singular ?plural n =
  if n = 0 then None
  else if n = 1 then Some ("1 " ^ singular)
  else
    let plural = match plural with
                 | None -> singular ^ "s"
                 | Some plural -> plural in
    Some (string_of_int n ^ " " ^ plural)

let rec take n xs =
  match xs with
  | _ when n = 0 -> []
  | x :: xs -> x :: take (n-1) xs
  | [] -> []
