set -xe
MYSERVER=kite
EXENAME=pl_anet.exe
EXE=_build/default/exe/$EXENAME
dune exec exe/$EXENAME
chmod u+w $EXE
rsync -avK template config output scripts/run.sh $EXE $MYSERVER:coq-planet/
