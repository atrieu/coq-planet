# The pl-a.net link aggregator

A link aggregator is a way to consolidate numerous and disparate communication
channels (reddit, discourse, zulip...) around a common topic of interest.

This project currently caters to the [Coq](https://coq.inria.fr) community.

- [Planet Coq](https://coq.pl-a.net)
- (Haskell version coming soon.)

Of course, the software can easily and freely be repurposed to other centers of
interest.

Contributions are welcome. Please report any issues with the site on
[the issue tracker](https://gitlab.com/lysxia/coq-planet/-/issues),
or via e-mail (lysxia@gmail.com).

## pl-a.net subdomains

The pl-a.net subdomains (*\<your-favorite-lang\>*.pl-a.net) are freely available
to serve programming language communities (that's why it's named "pl"-a.net).
Get in touch to suggest ideas! (For example, to add a redirection, or to host
a variant of this link aggregator.)

---

## Build

### Dependencies

Requires `ocaml >= 4.08`

```sh
opam install fileutils mustache ptime cohttp lwt cohttp-lwt-unix ezgzip yojson lambdasoup
```

### Build and run

First set some Zulip credentials in `config/secrets.json` (a template is provided in
`config/secrets.json.template`); or comment out the Zulip line(s) in the source file
`exe/pl_anet.ml`. (TODO: make this less hacky)

This generates a static site in `output/`.

```sh
dune exec exe/pl_anet.exe
```

Fetching from Coq Club is particularly slow. It can be skipped for faster
testing, using the option `--skip-coqclub`.

```sh
dune exec -- exe/pl_anet.exe --skip-coqclub
```

## Code organization

The executable is a simple static site generator.

1. Request data from the various sources, via an API when available, or
   scraping otherwise.
2. Format the most recent entries for each source.
3. Wrap it up in some HTML templates.

Repeat every 10 minutes or so, and voilà.

### Files

- Executable: `exe/pl_anet.ml`

- Sources (`lib/`)

    - `reddit.ml`
    - `stackoverflow.ml` (really all of StackExchange)
    - `zulip.ml`
    - `discourse.ml`
    - `coqclub.ml` (Coq mailing list)

- Utilities (`lib/`)

    - `common.ml`: types and utilities for processing sources
    - `template.ml`: mustache templates, file handling
    - `secrets.ml`: authentication management
    - `html.ml`: minimal library for HTML output
    - `opt.ml`: command-line options
    - `debug.ml`
    - `time.ml`

---

Inspiration: [Haskell News](https://github.com/haskellnews/haskellnews) (now defunct)
